<?php

namespace admin\controllers;

use admin\models\LabaRugi;
use Yii;
use common\models\User;
use admin\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class LaporanController extends Controller
{


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Public function Labarugi() For
     **/
    public function actionLabarugi()
    {
        $model = new LabaRugi();
        $labarugi = [];
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $labarugi = $model->getLabarugi();
        }

        return $this->render('labarugi',[
            'model' => $model,
            'labarugi' => $labarugi
        ]);
    }
}
