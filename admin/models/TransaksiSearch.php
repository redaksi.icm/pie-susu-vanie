<?php

namespace admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transaksi;

/**
 * TransaksiSearch represents the model behind the search form of `common\models\Transaksi`.
 */
class TransaksiSearch extends Transaksi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transaksi', 'tanggal', 'keterangan_transaksi', 'status', 'tipe_transaksi', 'no_bukti'], 'safe'],
            [['total_transaksi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaksi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tanggal' => $this->tanggal,
            'total_transaksi' => $this->total_transaksi,
        ]);

        $query->andFilterWhere(['like', 'id_transaksi', $this->id_transaksi])
            ->andFilterWhere(['like', 'keterangan_transaksi', $this->keterangan_transaksi])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'tipe_transaksi', $this->tipe_transaksi])
            ->andFilterWhere(['like', 'no_bukti', $this->no_bukti]);

        return $dataProvider;
    }
}
