<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AkunKlasifikasi */

$this->title = Yii::t('app', 'Update Akun Klasifikasi: {nameAttribute}', [
    'nameAttribute' => $model->klasifikasi,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akun Klasifikasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->klasifikasi, 'url' => ['view', 'id' => $model->klasifikasi]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="akun-klasifikasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
