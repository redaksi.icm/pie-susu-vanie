<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Pie Susu Vanie';

?>
<div class="site-index">
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= formatter()->asInteger($countPelanggan)?></div>
                            <div>Pelanggan</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= formatter()->asInteger($countOrder)?></div>
                            <div>Order</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-money fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= formatter()->asInteger($countOrderValidate)?></div>
                            <div>Pembayaran</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Statistik
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="morris-area-chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

<!--            <div class="text-center">-->
<!--                <img class="img-responsive center-block" src="--><?//= Url::base() . '/logo.jpeg'?><!--"/>-->
<!--            </div>            -->
        </div>
    </div>
</div>
<?php
$dataCartMoris = json_encode($dataChart);
$js = <<<JS
var dataMoris = $dataCartMoris;
Morris.Area({
        element: 'morris-area-chart',
        data: dataMoris,
        xkey: 'period',
        ykeys: ['pelanggan', 'order', 'pembayaran'],
        labels: ['Pelanggan', 'Order', 'Pembayaran'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
JS;

$this->registerJs($js);

?>