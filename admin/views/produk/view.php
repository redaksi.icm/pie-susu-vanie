<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Produk */

$this->title = $model->id_produk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Produks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="produk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_produk], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_produk], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_produk',
            'kategori.nama_kategori',
            'nama',
            [
                'attribute' => 'harga',
                'format' =>'raw',
                'value' => function($model){
                    return ($model->harga)?number_format($model->harga,2):null;
                }
            ],
            [
                'attribute' => 'size',
                'format' =>'raw',
                'value' => function($model){
                    return \yii\helpers\ArrayHelper::getValue(param('size'),$model->size);
                }
            ],
            [
                'attribute' => 'status',
                'format' =>'raw',
                'value' => function($model){
                    return \yii\helpers\ArrayHelper::getValue(param('status'),$model->status);
                }
            ],
            [
                'attribute' => 'images',
                'format' =>'raw',
                'value' => function($model){
                    return Html::img($model->getThumbUploadUrl('images'),['class' => 'img-thumbnail']);
                }
            ],
            'deskripsi:ntext',
        ],
    ]) ?>

</div>
