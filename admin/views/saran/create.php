<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Saran */

$this->title = Yii::t('app', 'Create Saran');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="saran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
