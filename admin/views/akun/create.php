<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Akun */

$this->title = Yii::t('app', 'Create Akun');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akuns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akun-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
