<?php

/* @var $this \yii\web\View */
/* @var $content string */

use admin\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
$user = Yii::$app->user->identity;
$namaUser = (Yii::$app->user->isGuest)? 'Guest': $user->nama_user;
if(!Yii::$app->user->isGuest){
    switch ($user->akses){
        case 'admin':
            $menuItems = [
                ['label' => 'Dashboard', 'url' => ['/site/index']],
                ['label' => 'Produk', 'url' => ['/produk/index']],
                ['label' => 'Ketegori', 'url' => ['/kategori-produk/index']],
                ['label' => 'Pelanggan', 'url' => ['/pelanggan/index']],
                ['label' => 'Order', 'url' => ['/order/index']],
                ['label' => 'Saran', 'url' => ['/saran/index']],
                ['label' => 'Slider', 'url' => ['/slider/index']],
                ['label' => 'User', 'url' => ['/user']],
                ['label' => 'Akunting', 'url' => ['#'],'items' => [
                    ['label' => 'Akun', 'url' => ['/akun']],
                    ['label' => 'Akun Klasifikasi', 'url' => ['/akun-klasifikasi']],
                ]],
                ['label' => 'Transaksi Pengeluaran', 'url' => ['/transaksi/pengeluaran']],
                ['label' => 'Laporan', 'url' => ['/laporan']],
            ];
            break;
        case 'user':
            $menuItems = [
                ['label' => 'Dashboard', 'url' => ['/site/index']],
                ['label' => 'Produk', 'url' => ['/produk/index']],
                ['label' => 'Pelanggan', 'url' => ['/pelanggan/index']],
                ['label' => 'Order', 'url' => ['/order/index']],
                ['label' => 'Transaksi Pengeluaran', 'url' => ['/transaksi/pengeluaran']],
                ['label' => 'Laporan','items' => [
                    ['label' => 'Pelanggan', 'url' => ['pelanggan/laporan']],
                    ['label' => 'Order', 'url' => ['order/laporan']],
                ]],
            ];
            break;
        case 'owner':
            $menuItems = [
                ['label' => 'Dashboard', 'url' => ['/site/index']],
                ['label' => 'Produk', 'url' => ['/produk/index']],
                ['label' => 'Pelanggan', 'url' => ['/pelanggan/index']],
                ['label' => 'Order', 'url' => ['/order/index']],
                ['label' => 'Saran', 'url' => ['/saran/index']],
                ['label' => 'Transaksi Pengeluaran', 'url' => ['/transaksi/pengeluaran']],
                ['label' => 'Laporan', 'url' => ['/laporan']],
            ];
            break;


    }

}else{
    $menuItems = [];
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top hidden-print" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""><?= Html::encode($this->title) ?></a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $namaUser?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <?php
                    if (Yii::$app->user->isGuest) {
                        ?>
                        <li><?= a('<i class="fa fa-fw fa-sign-in"></i> Login',['/site/login'])?></li>
                        <?php
                    } else {
                        ?>
                        <li><?= a('Profile',['/site/profile'])?></li>
                        <?php
                        echo '<li>'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                '<i class="fa fa-fw fa-power-off"></i> Log Out',
                                ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>';
                    }
                    ?>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <?php

            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav side-nav'],
                'items' => $menuItems,
            ]);
            ?>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <?= $content?>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
