<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */

$js = <<<JS
$('#order-tgl_order').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
JS;
$this->registerJs($js);
?>

<div class="order-form">
	

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_order')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_pelanggan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_order')->textInput() ?>

    <?= $form->field($model, 'total_harga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bukti_pembayaran')->fileInput()?>

    <?= $form->field($model, 'no_rekening')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metode_pembayaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_akun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_pembayaran')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
