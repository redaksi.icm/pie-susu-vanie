<?php

use yii\db\Schema;
use yii\db\Migration;

class m180206_143155_kategori_produkDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        $this->batchInsert('{{%kategori_produk}}',
                           ["id_kategori", "nama_kategori"],
                            [
    [
        'id_kategori' => 'D',
        'nama_kategori' => 'Donut',
    ],
    [
        'id_kategori' => 'PS',
        'nama_kategori' => 'Pie Susu',
    ],
]
        );
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%kategori_produk}} CASCADE');
    }
}
