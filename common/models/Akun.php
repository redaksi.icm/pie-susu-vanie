<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%akun}}".
 *
 * @property string $kode_rekening
 * @property string $nama_rekening
 * @property string $klasifikasi
 *
 * @property AkunKlasifikasi $akunKlasifikasi
 */
class Akun extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%akun}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_rekening', 'nama_rekening'], 'required'],
            [['kode_rekening'], 'string', 'max' => 10],
            [['nama_rekening'], 'string', 'max' => 100],
            [['klasifikasi'], 'string', 'max' => 1],
            [['kode_rekening'], 'unique'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkunKlasifikasi()
    {
        return $this->hasOne(AkunKlasifikasi::className(), ['klasifikasi' => 'klasifikasi']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode_rekening' => Yii::t('app', 'Kode Rekening'),
            'nama_rekening' => Yii::t('app', 'Nama Rekening'),
            'klasifikasi' => Yii::t('app', 'Klasifikasi'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AkunQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AkunQuery(get_called_class());
    }
}
