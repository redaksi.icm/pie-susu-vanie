<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%jurnal_detail}}".
 *
 * @property int $id_jurnal
 * @property string $kode_akun
 * @property string $debit
 * @property string $credit
 * @property int $id
 *
 * @property Jurnal $jurnal
 * @property Akun $akun
 */
class JurnalDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%jurnal_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jurnal'], 'integer'],
            [['debit', 'credit'], 'number'],
            [['kode_akun'], 'string', 'max' => 10],
            [['id_jurnal'], 'exist', 'skipOnError' => true, 'targetClass' => Jurnal::className(), 'targetAttribute' => ['id_jurnal' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jurnal' => Yii::t('app', 'Id Jurnal'),
            'kode_akun' => Yii::t('app', 'Kode Akun'),
            'debit' => Yii::t('app', 'Debit'),
            'credit' => Yii::t('app', 'Credit'),
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJurnal()
    {
        return $this->hasOne(Jurnal::className(), ['id' => 'id_jurnal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkun()
    {
        return $this->hasOne(Akun::className(), ['kode_rekening' => 'kode_akun']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\JurnalDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\JurnalDetailQuery(get_called_class());
    }
}
