<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%jurnal}}".
 *
 * @property int $id
 * @property string $id_transaksi
 * @property string $keterangan
 * @property string $tanggal
 *
 * @property JurnalDetail[] $jurnalDetails
 */
class Jurnal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%jurnal}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal'], 'safe'],
            [['id_transaksi'], 'string', 'max' => 20],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_transaksi' => Yii::t('app', 'Id Transaksi'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'tanggal' => Yii::t('app', 'Tanggal'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJurnalDetails()
    {
        return $this->hasMany(JurnalDetail::className(), ['id_jurnal' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\JurnalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\JurnalQuery(get_called_class());
    }
}
