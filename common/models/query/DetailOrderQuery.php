<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\DetailOrder]].
 *
 * @see \common\models\DetailOrder
 */
class DetailOrderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\DetailOrder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\DetailOrder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
