<?php

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/30/2018
 * Time: 10:35 PM
 */

//---------------SHORT CODE---------------//
/**
 * Fungsi untuk mendapatkan parameter dari config
 * @param $name string
 * @param null $default
 * @return mixed
 */
function param($name, $default = null)
{
    return ArrayHelper::getValue(Yii::$app->params, $name, $default);
}

/**
 * Fungsi untuk menambahkan parameter
 * @param $param
 * @param $value
 */
function addParam($param,$value){
    Yii::$app->params[$param] = $value;
}

/**
 * Fungsi untuk menggenarate URL
 * @param string $url
 * @param bool $scheme
 *
 * @return string
 */
function url($url = '', $scheme = false)
{
    return Url::to($url, $scheme);
}

/**
 * Fungsi untuk Encode Html string
 * @param $text
 *
 * @return string
 */
function hencode($text)
{
    return Html::encode($text);
}

/**
 * Fungsi untuk menggenerate Image Html
 * @param $src
 * @param array $opt
 *
 * @return string
 */
function img($src, $opt = []){
    return Html::img($src,$opt);
}

/**
 * Fungsi untuk menggenerate Html Link
 * @param $text
 * @param null $url
 * @param array $opt
 *
 * @return string
 */
function a($text, $url = null, $opt = []){
    return Html::a($text,$url,$opt);
}

function faIcon($icon, $options = [], $tag = 'i'){
    $options['class'] = isset($options['class'])? $options['class'] . ' ' . $icon: $icon;
    return Html::tag($tag,'',$options);
}

function userCan($permission){
    return Yii::$app->user->can($permission);
}

function formatter(){
    $formatter = Yii::$app->formatter;
    return $formatter;
}

function getUser(){
    if(!Yii::$app->user->isGuest){
        return Yii::$app->user->identity;
    }
}

/**
 * Fungsi untuk registrasi path storage sebagai asset
 * Disarankan untuk mengubah assetManager->linkAssets = true
 * @return mixed
 */
function registerStorage(){
    Yii::$app->assetManager->linkAssets = true;

    $view = Yii::$app->getView();
    $storage = \common\assets\StorageAsset::register($view);
    return $storage;
}

/**
 * @param null $path
 *
 * @return string
 */
function getstorageUrl($path = null){
    return Yii::getAlias('@storageUrl') . '/' . $path;
}

/**
 * @param null $path
 *
 * @return string
 */
function getStoragePath($path = null){
    return Yii::getAlias('@storage') . '/' . $path;
}

/**
 * Fungsi untuk membuat list array tahun
 * @param int $from
 * @return array
 */
function getYearsList($from = 2000) {
    $currentYear = date('Y');
    $yearFrom = $from;
    $yearsRange = range($yearFrom, $currentYear);
    return array_combine($yearsRange, $yearsRange);
}

function formatRupiah($number){
    return 'Rp. ' . number_format($number,2);
    //formatter()->locale = 'id_ID';
    //return Yii::$app->formatter->asCurrency($number,null,[
    //    NumberFormatter::MIN_FRACTION_DIGITS => 2,
    //    NumberFormatter::MAX_FRACTION_DIGITS => 2,
    //]);
}

function getCartTotal(){
    $cost = Yii::$app->cart->getCost();
    $count = Yii::$app->cart->getCount();
    if($count){
        return Yii::t('app','{count} item(s) - {total}',[
            'count' => $count,
            'total' => formatter()->asCurrency($cost)
        ]);
    }else{
        return 'Tidak ada pesanan';
    }
}
//---------------END SHORT CODE---------------//