var appLockScreen, bsWidget, detail, globalSearchModal, juiMessage, reloadGrid, reloadPjaxGrid, showModal;

if (typeof jQuery === "undefined") {
  throw new Error("This App Required Jquery");
} else {
  $.extend(Date.prototype, {
    addHours: function(h) {
      return this.setHours(this.getHours() + h);
    },
    addDay: function(d) {
      return this.setDay(this.getDay() + d);
    },
    addMonth: function(m) {
      return this.setMonth(this.getMonth() + m);
    }
  });
}

if (typeof Storage === "undefined") {
  throw new Error("Sorry! No Web Storage support..");
} else {

}

Date.prototype.addDays = function(days) {
  return this.setDate(this.getDate() + parseInt(days));
};

(function($) {
  return $.fn.enterAsTab = function(options) {
    var settings;
    settings = $.extend({
      'allowSubmit': false
    }, options);
    $(this).find('input, select, textarea, button').on("keydown", {
      localSettings: settings
    }, function(event) {
      var err, idx, inputs, type;
      if (settings.allowSubmit) {
        type = $(this).attr("type");
        if (type === "submit") {
          return true;
        }
      }
      if (event.keyCode === 13) {
        inputs = $(this).parents("form").eq(0).find(":input:visible:not(:disabled):not([readonly])");
        idx = inputs.index(this);
        if (idx === inputs.length - 1) {
          idx = -1;
        } else {
          inputs[idx + 1].focus();
        }
        try {
          inputs[idx + 1].select();
        } catch (error) {
          err = error;
        }
        return false;
      }
    });
    return this;
  };
})(jQuery);

$(function() {
  "use strict";
  var modal;
  modal = $("#global-modal");
  $(document).on('click', '.showModalButton', function(e) {
    e.preventDefault();
    showModal(modal, $(this).attr('title'), $(this).attr('value'));
  });
});

bsWidget = {
  alert: function(element, message_id, alertType, message) {
    $("#" + element).prepend("<div class=\"alert " + alertType + "\" role=\"alert\" id='" + message_id + "'>" + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" + "<span aria-hidden=\"true\">&times;</span>" + "</button>" + message + "</div>");
    $("#" + message_id).fadeTo(2000, 500).slideUp(500, function() {
      $("#" + message_id).slideUp(500, function() {
        $(this).remove();
      });
    });
  }
};

showModal = function(modalElement, title, contentURL) {
  var header;
  modalElement.find('#modalContent').html("<div style=\"text-align:center\"> <i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i> <span class=\"sr-only\">Loading...</span></div>");
  header = modalElement.find('#modalHeader').find('h4');
  if (header.html()) {
    header.html($(this).attr('title'));
  } else {
    modalElement.find('#modalHeader').append('<h4>' + title + '</h4>');
  }
  if ((modalElement.data().isShown)) {
    modalElement.find('#modalContent').load(contentURL);
  } else {
    modalElement.modal('show').find('#modalContent').load(contentURL);
  }
};

globalSearchModal = function(formElement, containerElement, overlayElement) {
  $(formElement).on('submit', function(e) {
    e.preventDefault();
    reloadGrid(formElement, containerElement, overlayElement);
  });
};

reloadGrid = function(formElement, containerElement, overlayElement) {
  var params;
  $(containerElement).on('pjax:beforeSend', function(e, jqXHR, settings) {
    $(overlayElement).toggleClass('hidden');
    $.pjax.defaults.data = $(formElement).serialize();
  }).on('pjax:end', function(e, jqXHR, settings) {
    $(overlayElement).toggleClass('hidden');
  });
  params = $(formElement).serialize();
  console.log(containerElement);
  $.pjax.reload({
    container: $(containerElement),
    type: 'POST',
    data: params
  });
};

detail = function(obj, person_id) {
  var a, table, td, tdCount, tdDetail, tr, trDetail;
  a = obj;
  td = $(a).parent();
  tr = $(td).parent();
  tdCount = $(tr).children().length;
  table = $(tr).parent();
  $(table).children(".trDetail").remove();
  trDetail = document.createElement("tr");
  $(trDetail).attr("class", "trDetail");
  tdDetail = document.createElement("td");
  $(tdDetail).attr("colspan", tdCount);
  $(tdDetail).html("<span class=\'fa fa-spinner fa-spin\'></span>");
  $.get("'.\yii\helpers\Url::to(['person/detail-person']).'?id=" + person_id, function(data) {
    $(tdDetail).html(data);
  }).fail(function() {
    alert("error");
  });
  $(trDetail).append(tdDetail);
  $(tr).after(trDetail);
};

juiMessage = function(output_msg, title_msg, button) {
  if (typeof jQuery.ui === "undefined") {
    throw new Error("This App Required Jquery UI");
  }
  if (!title_msg) {
    title_msg = 'Alert';
  }
  if (!output_msg) {
    output_msg = 'No Message to Display.';
  }
  if (!button) {
    button = {
      "Close": function() {
        $(this).dialog('close');
      }
    };
  }
  $('#jui_message').html(output_msg).dialog({
    title: title_msg,
    dialogClass: 'no-close',
    resizable: false,
    modal: true,
    buttons: button
  });
};

({

  /**
   * Get selected row in grid view
   */
  getSelectedGrid: function(gridId) {
    var grid;
    if (gridId instanceof jQuery) {
      grid = gridId;
    } else {
      grid = $("#" + gridId);
    }
    return grid.yiiGridView('getSelectedRows');
  }

  /**
   * Module Global Function
   */

  /**
   * reload grid using pjax
   */
});

reloadPjaxGrid = function(options) {
  var pjaxGridID, pjaxGridID_overlay, searchFormID;
  pjaxGridID = $('#' + options.pjaxGridID);
  pjaxGridID_overlay = $('#' + options.pjaxGridID + '_overlay');
  searchFormID = $('#' + options.searchFormID);
  $.pjax.reload({
    container: pjaxGridID,
    type: 'POST',
    data: searchFormID.serialize()
  });
  return pjaxGridID.on('pjax:beforeSend', function(e, jqXHR, settings) {
    pjaxGridID_overlay.toggleClass('hidden');
    $.pjax.defaults.data = searchFormID.serialize();
  }).on('pjax:end', function(e, jqXHR, settings) {
    pjaxGridID_overlay.toggleClass('hidden');
  }).on('pjax:complete', function(event) {
    var msg;
    if (options.msg !== 'undefined' && options.msg !== null) {
      msg = options.msg;
      if (msg.status !== 'undefined' && msg.status === "success") {
        bsWidget.alert(options.pjaxGridID, options.controller + "_msgalert", msg.elClass, msg.message);
        options.msg = null;
      }
    }
  });
};

appLockScreen = function(options) {
  var autoLockTimer, goLockScreen, init, stop;
  autoLockTimer = 0;
  goLockScreen = false;
  stop = false;
  init = {
    lockScreen: function() {
      stop = true;
      window.location.href = options.lockScreenUrl + '?previous=' + encodeURIComponent(window.location.href);
    },
    lockIdentity: function() {
      goLockScreen = true;
    },
    resetTimer: function() {
      if (stop === true) {

      } else if (goLockScreen === true) {
        init.lockScreen();
      } else {
        clearTimeout(autoLockTimer);
        autoLockTimer = setTimeout(init.lockIdentity, options.limitTimeout);
      }
    },
    run: function() {
      window.onload = init.resetTimer;
      window.onmousemove = init.resetTimer;
      window.onmousedown = init.resetTimer;
      window.onclick = init.resetTimer;
      window.onscroll = init.resetTimer;
      window.onkeypress = init.resetTimer;
    }
  };
  init.run();
};


/**
 * Button Checkbox
 */

(function($) {
  $(document).ready(function() {
    $('.tongle-button').click(function() {
      var elm;
      elm = $(this).data('tongle');
      $('.' + elm).slideToggle();
    });
  });
  $('.button-checkbox').each(function() {
    var $body, $button, $checkbox, $widget, color, init, navHeight, settings, updateDisplay;
    $widget = $(this);
    $button = $widget.find('button');
    $checkbox = $widget.find('input:checkbox');
    color = $button.data('color');
    settings = {
      on: {
        icon: 'glyphicon glyphicon-check'
      },
      off: {
        icon: 'fa fa-square-o'
      }
    };
    $button.on('click', function() {
      $checkbox.prop('checked', !$checkbox.is(':checked'));
      $checkbox.triggerHandler('change');
      updateDisplay();
    });
    $checkbox.on('change', function() {
      updateDisplay();
    });
    updateDisplay = function() {
      var isChecked;
      isChecked = $checkbox.is(':checked');
      $button.data('state', isChecked ? "on" : "off");
      $button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);
      if (isChecked) {
        $button.removeClass('btn-default').addClass('btn-' + color + ' active');
      } else {
        $button.removeClass('btn-' + color + ' active').addClass('btn-default');
      }
    };
    init = function() {
      updateDisplay();
      if ($button.find('.state-icon').length === 0) {
        $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
      }
    };
    init();
    return;
    $body = $(document.body);
    navHeight = $('.navbar').outerHeight(true) + 10;
    $('.search-job').affix({
      offset: {
        top: function() {
          var navOuterHeight;
          navOuterHeight = $('#job_list').height();
          return this.top = navOuterHeight;
        },
        bottom: function() {
          return this.bottom = $('footer').outerHeight(true);
        }
      }
    });
    return $body.scrollspy({
      target: '#leftCol',
      offset: navHeight
    });
  });
})(jQuery);
