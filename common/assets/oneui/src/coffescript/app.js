var App;

App = function() {
  var uiInit;
  uiInit = function() {
    var $lBody, $lFooter, $lHeader, $lHtml, $lMain, $lPage, $lSideOverlay, $lSideOverlayScroll, $lSidebar, $lSidebarScroll, uiLayout;
    $lHtml = jQuery('html');
    $lBody = jQuery('body');
    $lPage = jQuery('#page-container');
    $lSidebar = jQuery('#sidebar');
    $lSidebarScroll = jQuery('#sidebar-scroll');
    $lSideOverlay = jQuery('#side-overlay');
    $lSideOverlayScroll = jQuery('#side-overlay-scroll');
    $lHeader = jQuery('#header-navbar');
    $lMain = jQuery('#main-container');
    $lFooter = jQuery('#page-footer');
    jQuery('[data-toggle="tooltip"], .js-tooltip').tooltip({
      container: 'body',
      animation: false
    });
    jQuery('[data-toggle="popover"], .js-popover').popover({
      container: 'body',
      animation: true,
      trigger: 'hover'
    });
    jQuery('[data-toggle="tabs"] a, .js-tabs a').click(function(e) {
      e.preventDefault();
      jQuery(this).tab('show');
    });
    jQuery('.form-control').placeholder();
    uiLayout = function() {
      if ($lMain.length) {
        uiHandleMain();
        jQuery(window).on('resize orientationchange', function() {
          var $resizeTimeout;
          clearTimeout($resizeTimeout);
          return $resizeTimeout = setTimeout(function() {
            uiHandleMain();
          }, 150);
        });
      }
    };
  };
};
