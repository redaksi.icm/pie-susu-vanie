<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/13/2017
 * Time: 9:03 AM
 */

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\models\SearchJob;
use yii\bootstrap\Html;
use common\assets\oneui\OneUiAsset;

$this->title = Yii::$app->name;
$params = Yii::$app->params;

$searchJob = new SearchJob();

$mainAsset = OneUiAsset::register($this);
$baseAssetUrl = \yii\helpers\Url::to($mainAsset->baseUrl,true);

$storageAsset = \common\assets\StorageAsset::register($this);
$storageAssetUrl = $storageAsset->basePath;
$storageUrl = $storageAsset->baseUrl;

$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : null;

$js = <<<JS
$(function(){App.initHelpers(['appear']);});

JS;

$css = <<<CSS
#search-form{
    color: #ffffff;    
}

#search-form > .form-group {
    font-size: large;
}


CSS;


$this->registerCss($css);
$this->registerJs($js);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= Html::encode($this->title) ?></title>
    <?= \common\widgets\favicon\Pavicon::widget([
        'web' => '@assetUrl/favicon',
        'webroot' => '@assetCache/favicon',
        'favicon' => "$storageAssetUrl/img/logo-disnaker.png",
        'color' => '#2b5797',
        'viewComponent' => 'view',
    ])?>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>

</head>
<body class="<?= $this->params['body-class']?>">
<?php $this->beginBody() ?>
<!-- Page Container -->
<!--
    Available Classes:

    'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

    'sidebar-l'                  Left Sidebar and right Side Overlay
    'sidebar-r'                  Right Sidebar and left Side Overlay
    'sidebar-mini'               Mini hoverable Sidebar (> 991px)
    'sidebar-o'                  Visible Sidebar by default (> 991px)
    'sidebar-o-xs'               Visible Sidebar by default (< 992px)

    'side-overlay-hover'         Hoverable Side Overlay (> 991px)
    'side-overlay-o'             Visible Side Overlay by default (> 991px)

    'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

    'header-navbar-fixed'        Enables fixed header
    'header-navbar-transparent'  Enables a transparent header (if also fixed, it will get a solid dark background color on scrolling)
-->
<div id="page-container" class="header-navbar-fixed header-navbar-transparent">

    <!-- Main Container -->
    <main id="main-container">
        <?= $content ?>
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="bg-white">
        <div class="content content-boxed">
            <!-- Footer Navigation -->
            <div class="row push-30-t items-push-2x">
                <div class="col-sm-8">
                    <h3 class="h5 font-w600 text-uppercase push-20">Hubungi Kami</h3>
                    <table>
                        <tr>
                            <td width="100px">Alamat</td>
                            <td><?= getConfig('alamat')?></td>
                        </tr>
                        <tr>
                            <td>No Telp</td>
                            <td><?= getConfig('info_phone')?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><?= getConfig('support_email')?></td>
                        </tr>
                        <tr>
                            <td>Pengaduan</td>
                            <td><?= a('Link Pengaduan',['/site/contact'])?></td>
                        </tr>

                    </table>
                </div>
                <div class="col-sm-4">
                    <h3 class="h5 font-w600 text-uppercase push-20">Statistik Pengunjung</h3>
                    <div class="font-s12 push">
                        <i class="si si-users"></i> Online <?php echo Yii::$app->userCounter->getOnline(); ?><br>
                        <i class="si si-users"></i> Saat Ini <?php echo Yii::$app->userCounter->getToday(); ?><br>
                        <i class="si si-users"></i> Kemarin <?php echo Yii::$app->userCounter->getYesterday(); ?><br>
                        <i class="fa fa-calendar"></i> Bulan Ini <?php echo Yii::$app->userCounter->getThisMonth(); ?><br>
                        <i class="si si-calendar"></i> Bulan Lalu <?php echo Yii::$app->userCounter->getLastMonth(); ?><br>
                        <i class="si si-users"></i> <b>Total Pengunjung <?php echo Yii::$app->userCounter->getTotal(); ?></b><br>
                        <hr>
                        <i class="si si-users"></i> Tgl Terbanyak <?php echo formatter()->asDate(Yii::$app->userCounter->getMaximalTime()); ?><br>
                        <i class="si si-users"></i> Terbanyak <?php echo Yii::$app->userCounter->getMaximal(); ?><br>


                    </div>
                </div>
            </div>
            <!-- END Footer Navigation -->

            <!-- Copyright Info -->
            <div class="font-s12 push-20 clearfix">
                <hr class="remove-margin-t">
                <div class="pull-right">
                    Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">OneUI 3.1</a> &copy; <span class="js-year-copy"></span>
                </div>
            </div>
            <!-- END Copyright Info -->
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->
<?php $this->endBody() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'global-modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
//        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'>
<div style=\"text-align:center\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
<span class=\"sr-only\">Loading...</span></div>
</div>";
yii\bootstrap\Modal::end();
?>
</body>
</html>
<?php $this->endPage() ?>
