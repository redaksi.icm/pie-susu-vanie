<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/12/2018
 * Time: 2:08 PM
 */

namespace common\assets\oneui\widget\Calendar;


use yii\web\AssetBundle;

class MomentAsset extends AssetBundle
{

    /**
     * [$sourcePath description]
     * @var string
     */
    public $sourcePath = '@common/assets/oneui/src';

    /**
     * [$js description]
     * @var array
     */
    public $js = [
        'js/plugins/fullcalendar/moment.min.js',
    ];

}