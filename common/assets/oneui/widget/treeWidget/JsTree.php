<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/10/2018
 * Time: 3:43 PM
 */

namespace common\assets\oneui\widget\treeWidget;


use yii\base\Widget;
use yii\helpers\ArrayHelper;

class JsTree extends Widget
{

    public $template = "<div class=\"block\">
                <div class=\"block-header bg-gray-lighter\">
                    <ul class=\"block-options\">
                        <li>
                            <button type=\"button\"><i class=\"si si-settings\"></i></button>
                        </li>
                    </ul>
                    <h3 class=\"block-title\">{title}</h3>
                </div>
                <div class=\"block-content\">
                    {tree}
                </div>
            </div>";
    public $title = "Kategori Pekerjaan";
    public $initial = 'js-tree-collapsed';
    public $options = [];
    public $data = [];
    public $clientOptions = [];
    private $defaultOptions = [
        'color'=> '#555',
        'expandIcon'=> 'fa fa-plus',
        'collapseIcon'=> 'fa fa-minus',
        'nodeIcon'=> 'fa fa-folder text-primary-light',
        'onhoverColor'=> '#f9f9f9',
        'selectedColor'=> '#555',
        'selectedBackColor'=> '#f1f1f1',
        'showTags'=> true,
        'levels'=> 1
    ];

    public function GenerateJs()
    {

        $view = $this->getView();
        OneUiTreeWidgetAsset::register($view);
        $this->defaultOptions['data'] = $this->data;
        $container = "jQuery('#".$this->options['id']."').treeview(".json_encode($this->defaultOptions).")";
//        $js = <<<JS
//        jQuery('#').treeview({
//            data: $data,
//            color: '#555',
//            expandIcon: 'fa fa-plus',
//            collapseIcon: 'fa fa-minus',
//            nodeIcon: 'fa fa-folder text-primary-light',
//            onhoverColor: '#f9f9f9',
//            selectedColor: '#555',
//            selectedBackColor: '#f1f1f1',
//            showTags: true,
//            levels: 1
//        });
//JS;


        $view->registerJs($container);

    }

    public function run()
    {

        $clOption = isset($this->clientOptions)? $this->clientOptions:null;
        if($clOption){
            $this->defaultOptions = ArrayHelper::merge($this->defaultOptions,$clOption);
        }

        if (empty($this->options['id'])) {
            $this->options['id'] = 'js-tree'.$this->getId();
        }

        $this->GenerateJs();

        return strtr($this->template,[
            '{title}' => $this->title,
            '{tree}' => "<div id='".$this->options['id']."' class=\"js-tree-collapsed\"></div>",
        ]);


    }

}