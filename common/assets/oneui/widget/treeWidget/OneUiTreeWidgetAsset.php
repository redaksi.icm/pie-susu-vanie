<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/10/2018
 * Time: 3:38 PM
 */
namespace common\assets\oneui\widget\treeWidget;

use yii\web\AssetBundle;

class OneUiTreeWidgetAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/oneui/src';

    public $js = [
        'js/plugins/bootstrap-treeview/bootstrap-treeview.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}