<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/22/2018
 * Time: 1:51 PM
 */

namespace common\assets\oneui\widget\Marque;

use yii\base\Exception;
use yii\base\Widget;
use yii\bootstrap\Html;
use yii\data\DataProviderInterface;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;

class Marque extends Widget
{
    /**
     * @var array options to call an event such as "init", "destroy", etc..
     */
    public $events = [];

    public $direction = 'horizontal';

    /**
     * @var array options to populate Slick jQuery object
     */
    public $clientOptions = [
        'direction' => 'horizontal',
        'delay'=>0,
        'timing'=>50
    ];
    /**
     * @var integer position for inclusion javascript widget code to web page
     * @link http://www.yiiframework.com/doc-2.0/yii-web-view.html#registerJs()-detail
     */
    public $jsPosition = View::POS_READY;
    /**
     * @var array HTML attributes to render on the container
     */
    public $containerOptions = [];
    /**
     * @var string HTML tag to render the container
     */
    public $containerTag = 'ul';
    /**
     * @var string HTML tag to render items for the carousel
     */
    public $itemContainer = 'li';
    /**
     * @var array HTML attributes for the one item
     */
    public $itemOptions = [];

    /**
     * @var $dataProfider DataProviderInterface
     */
    public $dataProfider = null;
    /**
     * @var array elements for the carousel
     */
    public $items = [];

    public $viewParams = '';

    /**
     * @var $view View
     */
    private $view;

    public function init()
    {
        $this->view = $this->getView();
        $this->normalizeOptions();
        // not allowed empty Items
        if(empty($this->items)) {
            throw new Exception('Not allowed without items');
        }
    }

    protected function normalizeOptions()
    {
        // not allowed empty container
        !$this->containerTag && $this->containerTag = 'div';
        if(!isset($this->containerOptions['id']) || empty($this->containerOptions['id'])) {
            $this->containerOptions['id'] = $this->getId();
        }
    }


    public function registerClientScript()
    {
        MarqueAsset::register($this->view);
        $options = Json::encode($this->clientOptions);
        $id = $this->containerOptions['id'];
        $js[] = ";";
        $js[] = "jQuery('#$id').marquee($options);";
        $this->view->registerJs(implode(PHP_EOL, $js), $this->jsPosition);
        foreach ($this->events as $key => $value) {
            $this->view->registerJs(new JsExpression("$('#".$this->id."').on('".$key."', ".$value.""), $this->jsPosition);
        }
    }

    public function run()
    {
        $slider = Html::beginTag($this->containerTag, $this->containerOptions);
//        foreach($this->items as $item) {
//            $slider .= Html::tag($this->itemContainer, $item, $this->itemOptions);
//        }
        $slider .= $this->renderItems();
        $slider .= Html::endTag($this->containerTag);
        echo $slider;
        $this->registerClientScript();
    }

    public function renderItems()
    {
        $slider = null;
        if($this->dataProfider){
            $model = $this->dataProfider->getModels();
            $keys = $this->dataProvider->getKeys();
            foreach (array_values($model) as $index => $model) {
                $key = $keys[$index];
                $item = $this->renderItem($model, $key, $index);
                $slider .= Html::tag($this->itemContainer, $item, $this->itemOptions);
            }

        }else{
            foreach($this->items as $item) {
                $slider .= Html::tag($this->itemContainer, '<div style="width: 300px; height: 150px">'.$item . '</div>', $this->itemOptions);
            }
        }

        return $slider;
    }

    /**
     * @param $model
     * @param $key
     * @param $index
     * @return mixed|null|string
     */
    public function renderItem($model, $key, $index)
    {
        $content = null;
        if(is_string($this->items)){
            $content = $this->getView()->render($this->items, array_merge([
                'model' => $model,
                'key' => $key,
                'index' => $index,
                'widget' => $this,
            ], $this->viewParams));
        }else{
            $content = call_user_func($this->items, $model, $key, $index, $this);
        }

        return '<div style="width: 120px; height: 150px">'.$content.'</div>';
    }



}