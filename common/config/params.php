<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'app_name' => 'Pie Susu Vanie',
    'biaya_pengiriman' => 20000,
    'grid_count' => [
        5 => '5',
        10 => '10',
        20 => '20',
        50 => '50',
        100 => '100',
        150 => '150',
    ],
    'jenis_kelamin' => [
        'laki-laki' => Yii::t('fe','Laki-Laki'),
        'perempuan' => Yii::t('fe','Perempuan'),
    ],
    'metode_pembayaran' => [
        1 => Yii::t('fe','Transfer Lewat ATM'),
        2 => Yii::t('fe','Transfer Lewat Teller'),
    ],
    'size' => [
        1=>'Small',
        2=>'Medium',
        3=>'Large',
    ],
    'status' => [
        0 => 'Tidak Tersedia',
        1 => 'Tersedia',
    ],
    'status_aktif' => [
        0 => 'Tidak Aktif',
        1 => 'Aktif',
    ],
    'bulan_indo' => [
        'long' => ['Januari','Februari','Maret','April','Mei','Juni','Juli','agustus','September','Oktober','November','Desember'],
        'short' => ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Oct','Nov','Des']
    ],
    'hari_indo' => [
        'long' => ['Minggu','Senin', 'Selasa','Rabu','Kamis',"Jum'at",'Sabtu'],
        'short' => ['Ming','Sen', 'Sel','Rab','Kam',"Jum",'Sab'],
    ],
    'akses' => [
        'admin' => 'Administrator',
        'user' => 'User Input',
        'owner' => 'Pemilik',
    ]
];
