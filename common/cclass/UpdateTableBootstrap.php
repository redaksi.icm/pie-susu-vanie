<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 16/04/2018
 * Time: 15:19
 * Project: pie-susu-vanie
 */

namespace common\cclass;


use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\db\mysql\Schema;
use yii\db\QueryBuilder;
use yii\helpers\ArrayHelper;

class UpdateTableBootstrap implements BootstrapInterface
{

    private $db;

    public function __construct()
    {
        $this->db = Yii::$app->db;
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $this->updateDB();
    }

    function addColumn($table,$column, $dataType){
        Yii::beginProfile('UpdateDB');
        $builder = $this->db->createCommand();
        $tableSchema = $this->db->schema->getTableSchema($table);
        if(is_null($tableSchema->getColumn($column))){
            $builder->addColumn($table,$column,$dataType);
            $builder->execute();
        };
        Yii::endProfile('UpdateDB');
    }

    function createTable($table,$columns){
        Yii::beginProfile('CreateTable');
        $queryBuilder = new QueryBuilder($this->db);
        $cols = [];
        foreach ($columns as $name => $type) {
            if (is_string($name)) {
                $cols[] = "\t" . $this->db->quoteColumnName($name) . ' ' . $queryBuilder->getColumnType($type);
            } else {
                $cols[] = "\t" . $type;
            }
        }
        $sql = 'CREATE TABLE IF NOT EXISTS ' . $this->db->quoteTableName($table) . " (\n" . implode(",\n", $cols) . "\n)";

        $builder = Yii::$app->db->createCommand($sql);
        $builder->execute();
        Yii::endProfile('CreateTable');
    }


    /**
     * Public function addFk() For
     **/
    public function addFk($name,$table,$column,$refTable,$refColumn,$onDelete = 'CASCADE',$onUpdate = 'CASCADE')
    {
        Yii::beginProfile('UpdateFK');
        $tableSchema = Yii::$app->db->schema->getTableSchema($table);
        Yii::trace(ArrayHelper::keyExists($name,$tableSchema->foreignKeys),'Fk');
        $builder = Yii::$app->db->createCommand();
        if(ArrayHelper::keyExists($name,$tableSchema->foreignKeys)){
            $builder->dropForeignKey($name,$table);
            $builder->execute();
        }
        $builder->addForeignKey($name,$table,$column,$refTable,$refColumn,$onDelete,$onUpdate);
        $builder->execute();
        Yii::endProfile('UpdateFK');
    }

    /**
     * Public function updateDB() For
     **/
    public function updateDB()
    {
        Yii::beginProfile('UpdateDB');
        $transaction = Yii::$app->db->beginTransaction();
        try{
            $this->addColumn('order','nama','string');
            $this->addColumn('order','alamat','string');
            $this->addColumn('order','no_telp','VARCHAR(20)');
            $this->addColumn('order','ongkir','DECIMAL(15,2)');
            $this->addColumn('order','kota','string');
            $this->addColumn('order','provinsi','string');
            $this->addColumn('order','provinsi','string');
            $this->addColumn('order','kode_pos','VARCHAR(10)');
            $this->addColumn('pelanggan','status','int');
            $this->addColumn('pelanggan','kota','string');
            $this->addColumn('pelanggan','provinsi','string');
            $this->addColumn('pelanggan','kode_pos','VARCHAR(10)');
            $this->addColumn('pelanggan','tgl_registrasi','datetime');

            $this->createTable('transaksi',[
                'id_transaksi' => 'varchar(20) NOT NULL',
                'tanggal' => Schema::TYPE_DATE,
                'keterangan_transaksi' => 'varchar(100) NOT NULL',
                'total_transaksi' => Schema::TYPE_DECIMAL . '(15,2) NOT NULL',
                '`status` enum(\'Lunas\',\'Belum Lunas\')',
                '`tipe_transaksi` enum(\'Masuk\',\'Keluar\')',
                'no_bukti' => 'varchar(100)',
                'PRIMARY KEY (`id_transaksi`) USING BTREE'
            ]);

            $this->createTable('jurnal',[
                'id' => 'int(11) NOT NULL AUTO_INCREMENT',
                'id_transaksi' => 'varchar(20) NOT NULL',
                'keterangan' => 'varchar(255)',
                'tanggal' => Schema::TYPE_DATE,
                'images' => 'varchar(255)',
                'PRIMARY KEY (`id`) USING BTREE'
            ]);

            $this->createTable('jurnal_detail',[
                'id' => 'int(11) NOT NULL AUTO_INCREMENT',
                'id_jurnal' => 'int(11)',
                'kode_akun' => 'varchar(10)',
                'debit' => Schema::TYPE_DECIMAL . '(15,2) NOT NULL DEFAULT 0',
                'credit' => Schema::TYPE_DECIMAL . '(15,2) NOT NULL DEFAULT 0',
                'PRIMARY KEY (`id`) USING BTREE',
                'INDEX `fk_jurnal_detail_jurnal_1`(`id_jurnal`) USING BTREE',
                'INDEX `fk_jurnal_detail_akun_1`(`kode_akun`) USING BTREE',

            ]);



            $this->addFk('jurnal_ibfk_1','jurnal','id_transaksi','transaksi','id_transaksi');
            $this->addFk('fk_jurnal_detail_id_jurnal','jurnal_detail','id_jurnal','jurnal','id');
            $this->addFk('fk_jurnal_detail_kode_akun','jurnal_detail','kode_akun','akun','kode_rekening');

            $builder = $this->db->createCommand();
            $builder->delete('akun','kode_rekening=\'5.5\'')->execute();
            $builder->insert('akun',[
                'kode_rekening' => '5.5',
                'nama_rekening' => 'Beban Lain-Lain',
                'klasifikasi' => 'F',
            ])->execute();

            $transaction->commit();
        }catch (\Exception $e ){
            Yii::error($e->getMessage(),'Update');
            $transaction->rollBack();
        }

        Yii::endProfile('UpdateDB');
    }
}