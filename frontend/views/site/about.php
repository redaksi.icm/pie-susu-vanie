<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <!--Middle Part Start-->
    <div id="content" class="col-xs-12">
        <!-- Slideshow Start-->
        <div class="slideshow single-slider owl-carousel">
            <?php
            $slider = \common\models\ImageSlider::find()->all();
            if($slider){
                foreach($slider as $slide){
                    ?>
                    <div class="item"> <a href="#"><img class="img-responsive" src="<?= $slide->getUploadUrl('image')?>" /></a> </div>
                    <?php
                }
            }
            ?>
        </div>
        <!-- Slideshow End-->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h1>Tentang Pie Susu Vanie</h1>
        <p style="word-wrap: break-word">Pie Susu Vanie adalah oleh-oleh khas Bali yang didirikan oleh Deddy Chandra Kusuma. Pie Susu Vanie telah didirikan selama 4-5 tahun. Pie Susu Vanie telah pernah melakukan pengiriman baik dalam kota maupun di luar kota. Selama ini Pie Susu Vanie selalu menyediakan pie susu yang fresh dan enak agar dinikmati oleh pelanggan.</p>
    </div>
</div>
