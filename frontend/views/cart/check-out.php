<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/31/2018
 * Time: 1:45 AM
 * @var $this \yii\web\View
 * @var $model \common\models\Order
 */

use yii\bootstrap\Html;
use yii\grid\GridView;

$this->title = Yii::t('app','Check Out');
?>

<div class="row">
    <!--Middle Part Start-->
    <div id="content" class="col-sm-12">
        <h1 class="title">Checkout</h1>
        <?php
        if(Yii::$app->session->getFlash('error-order')){
            echo \yii\bootstrap\Html::errorSummary(Yii::$app->session->getFlash('error-order'));
        }
        ?>
        <?php
        $form = \yii\bootstrap\ActiveForm::begin([
            'action' => ['proses-order'],
//            'method' => 'POST'
        ]);
        ?>
        <?= $form->field($model,'ongkir')->hiddenInput()->label(false);?>
        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-user"></i> Your Personal Details</h4>
                    </div>
                    <div class="panel-body">
                        <fieldset id="account">
                            <?= $form->field($model,'nama')->textInput();?>
                            <?= $form->field($model,'no_telp')->textInput();?>
                        </fieldset>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-book"></i> Your Address</h4>
                    </div>
                    <div class="panel-body">
                        <fieldset id="address" class="required">

                            <?= $form->field($model,'alamat')->textarea();?>
                            <?= $form->field($model,'kota')->textInput();?>
                            <?= $form->field($model,'provinsi')->textInput();?>
                            <?= $form->field($model,'kode_pos')->textInput();?>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-shopping-cart"></i> Shopping cart</h4>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <?php
                                    if(Yii::$app->cart->getCount()){
                                        ?>
                                        <div class="table-responsive">
                                            <?= GridView::widget([
                                                'dataProvider' => $dataProvider,
                                                'layout' => '{items}',
                                                'columns' => [
                                                    [
                                                        'attribute' => 'images',
                                                        'label' => Yii::t('app', 'Foto'),
                                                        'format' => 'html',
                                                        'value' => function ($model) {
                                                            return img($model->getThumbUploadUrl('images'),['width' => 75, 'class' =>'img-thumbnail']);
                                                        },
                                                        'contentOptions' => [
                                                            'style' => 'width: 15%',
                                                            'class'=>"text-center"
                                                        ],
                                                    ],
                                                    [
                                                        'attribute' => 'nama',
                                                        'label' => Yii::t('app', 'Nama Produk'),
                                                        'format' => 'html',
                                                        'value' => function ($model) {
                                                            return a($model->nama, ['/product/view', 'id' => $model->id_produk]) . '<br>'.'<p>'.$model->kategori->nama_kategori.'</p>';
                                                        },
                                                    ],
                                                    [
                                                        'format' => 'raw',
                                                        'attribute' => 'quantity',
                                                        'label' => 'Quantity',
                                                        'headerOptions' => [
                                                            'width' => '80px'
                                                        ],
                                                        'contentOptions' => [
                                                            'class'=>"text-right"
                                                        ],
                                                        'value' => function ($model) {
                                                            return $model->getQuantity();
                                                        },
                                                    ],[
                                                        'attribute' => 'price',
                                                        'format' => 'currency',
                                                        'headerOptions' => [
                                                            'width' => '120px'
                                                        ],
                                                        'contentOptions' => [
                                                            'class'=>"text-right"
                                                        ],
                                                        'label' => Yii::t('app', 'Harga'),
                                                    ],
                                                    [
                                                        'label' => 'Total',
                                                        'format' => 'currency',
                                                        'headerOptions' => [
                                                            'width' => '120px'
                                                        ],
                                                        'contentOptions' => [
                                                            'class'=>"text-right"
                                                        ],
                                                        'value' => function ($model) {
                                                            return $model->getCost();
                                                        }
                                                    ]
                                                ],
                                            ])?>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-8">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td class="text-right"><strong>Sub Total:</strong></td>
                                                        <td class="text-right" id="txtsub_total"><?= Yii::$app->formatter->asCurrency(Yii::$app->cart->getCost()) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><strong>Biaya Pengiriman</strong></td>
                                                        <td class="text-right" id="txtongkir"><?= formatter()->asCurrency($model->ongkir)?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><strong>Total:</strong></td>
                                                        <td class="text-right" id="txttotal"><?= Yii::$app->formatter->asCurrency($model->ongkir + Yii::$app->cart->getCost()) ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <?php
                                    }else{
                                        echo Html::tag('h3','Maaf anda tidak memiliki produk di keranjang belanja');
                                    }
                                    ?>

                                </div>
                                <div class="buttons">
                                    <div class="pull-right">
                                        <?= a('Update Cart',['index'],['class' => 'btn btn-success'])?>
                                        <input type="submit" class="btn btn-primary" id="button-confirm" value="Confirm Order">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php \yii\bootstrap\ActiveForm::end()?>
    </div>
    <!--Middle Part End -->
</div>
    
