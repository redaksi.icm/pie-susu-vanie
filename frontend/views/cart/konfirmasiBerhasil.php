<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 11/04/2018
 * Time: 7:05
 * Project: pie-susu-vanie
 * @var $this \yii\web\View
 * @var $model \common\models\Order
 */

?>

<div class="row">
    <div class="col-md-12 text-center">
        <h1>Konfirmasi Pembayaran Berhasil</h1>
        <p>Konfirmasi pembayaran anda telah terkirim. Informasi pembayaran akan kami cek untuk validasi pesanan anda.</p>
    </div>
</div>
