<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/13/2017
 * Time: 9:03 AM
 */

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use common\assets\oneui\OneUiAsset;

$this->title = Yii::$app->name;

$mainAsset = OneUiAsset::register($this);
$baseAssetUrl = \yii\helpers\Url::to($mainAsset->baseUrl,true);

$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : null;

$js = <<<JS
$(function(){App.initHelpers(['appear']);});

JS;
$this->registerJs($js);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>

</head>
<body class="<?= param('body-class')?>">
<?php $this->beginBody() ?>
<div id="page-container" class="header-navbar-fixed header-navbar-transparent">

    <!-- Main Container -->
    <main id="main-container">
        <?= $content ?>
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="bg-white">
        <div class="content content-boxed">

        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->
<?php $this->endBody() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'global-modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
//        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'>
<div style=\"text-align:center\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
<span class=\"sr-only\">Loading...</span></div>
</div>";
yii\bootstrap\Modal::end();
?>
</body>
</html>
<?php $this->endPage() ?>
