<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProdukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = param('app_name') . ' | ' . Yii::t('app', 'Produks');
$this->params['breadcrumbs'][] = $this->title;
$sortUrl = \yii\helpers\Url::to(['data-sort']);
$dataLimitUrl = \yii\helpers\Url::to(['data-limit']);
$css = <<<CSS
.dx-loading {
    opacity: 0.5;
    content: 'Loading ...';
}

.dx-loading tr, .dx-loading td {
    background: transparent !important;
}

.dx-loading td {
    border-color: #efefef !important;
}
CSS;
$this->registerCss($css);
$js = <<<JS
$('#input-sort').on('change',function() {
    let vData = $(this).val();
    $.post('$sortUrl',{'sort': vData},function(result) {
      
    });  
});
$('#input-limit').on('change',function() {
    let vData = $(this).val();
    $.post('$dataLimitUrl',{'data-limit': vData},function(result) {
      $.pjax.reload({
        container: '#product-pjax',
        type: 'GET',        
      });
    });  
});

$('#product-pjax').on('pjax:beforeSend', function(e, jqXHR, settings) {
    $('#product-pjax-overlay').toggleClass('hidden');
}).on('pjax:end', function(e, jqXHR, settings) {
    $('#product-pjax-overlay').toggleClass('hidden');
}).on('pjax:send', function(e, jqXHR, settings) {
    $('#list_produk').addClass('dx-loading');
}).on('pjax:complete', function(e, jqXHR, settings) {
    $('#list_produk').removeClass('dx-loading');
});

JS;

$this->registerJs($js);
?>
<div class="produk-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?= Alert::widget();?>
    <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-xs-12">
            <div class="product-filter">
                <div class="row">
                    <div class="col-md-4 col-sm-5">
                        <div class="btn-group">
                            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                        </div>
                    </div>
                    <!--
                    <div class="col-sm-2 text-right">
                        <label class="control-label" for="input-sort">Sort By:</label>
                    </div>

                    <div class="col-md-3 col-sm-2 text-right">
                        <select id="input-sort" class="form-control col-sm-3">
                            <option value="" selected="selected">Default</option>
                            <option value="name-asc">Name (A - Z)</option>
                            <option value="name-desc">Name (Z - A)</option>
                            <option value="price-asc">Price (Low &gt; High)</option>
                            <option value="price-desc">Price (High &gt; Low)</option>
                            <option value="rating-desc">Rating (Highest)</option>
                            <option value="rating-asc">Rating (Lowest)</option>
                            <option value="model-asc">Model (A - Z)</option>
                            <option value="model-desc">Model (Z - A)</option>
                        </select>
                    </div>
                    -->

                        <div class="col-sm-1 text-right">
                            <label class="control-label" for="input-limit">Show:</label>
                        </div>
                        <div class="col-sm-2 text-right pull-right">
                            <?= Html::dropDownList('input-limit',Yii::$app->session->get('product-data-limit',10),param('grid_count'),['class'=> 'form-control','id' => 'input-limit'])?>
                        </div>
                </div>
            </div>
            <br />
            <?php \yii\widgets\Pjax::begin([
                'id' => 'product-pjax',
//                'enablePushState' => false,
                'linkSelector' => 'a:not(.linksWithTarget)',

            ])?>
            <?= ListView::widget([
                'id' => 'list_produk',
//            'pjax' => true,
//            'pjaxSettings' => [
//                'neverTimeout' =>true,
//            ],
                'options' => [
                    'tag' => 'div',
                ],
                'dataProvider' => $dataProvider,
                'itemView' => function ($model, $key, $index, $widget) {
                    $itemContent = $this->render('_list_produk',['model' => $model]);

                    return $itemContent;
                },
                'itemOptions' => [
                    'tag' => false,
                ],
                'layout' => "<div class='row products-category'>
                <div class='row'>                    
                    <div class='col-sm-6 text-left'>{pager}</div>
                    <div class='col-sm-6 text-right'>{summary}</div>
                </div>
                \n{items}\n
            </div>",
//            'summary' => '',
                'pager' => [
                    'firstPageLabel' => '<<',
                    'nextPageLabel' =>'>',
                    'prevPageLabel' =>'<',
                    'lastPageLabel' =>'>>',
                    'maxButtonCount' => 4,
                    'options' => [
                        'class' => 'pagination pagination-sm'
                    ]
                ],
            ])?>
            <?php \yii\widgets\Pjax::end()?>
        </div>
    </div>
</div>
