<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'language' => 'id',
    'bootstrap' => [
        'log',
//        'common\widgets\hcCart\CartBootstrap'
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'frontend\models\UserFront',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
//        'cart' => [
//            'class' => 'common\widgets\shoppingCart\Cart',
//            // you can change default storage class as following:
//            'storageClass' => [
//                'class' => 'common\widgets\shoppingCart\storage\DatabaseStorage',
//                // you can also override some properties
//                'deleteIfEmpty' => true,
//                'table' => 'cart',
//            ]
//
////            'class' => 'common\widgets\hcCart\Cart',
////            'storage' => [
////                'class' => 'common\widgets\hcCart\SessionStorage'
////            ]
//
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
