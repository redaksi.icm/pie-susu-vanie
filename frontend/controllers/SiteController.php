<?php
namespace frontend\controllers;

use admin\models\ProdukSearch;
use common\models\Saran;
use frontend\models\SaranSearch;
use frontend\models\UserFront;
use Yii;
use yii\base\InvalidParamException;
use yii\console\controllers\MigrateController;
use yii\db\Connection;
use yii\db\QueryBuilder;
use yii\db\Schema;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @var $db Connection
     */
    private $db;

    public function init()
    {
        $this->db = Yii::$app->db;
        parent::init();
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $produk = new ProdukSearch();
        $produkProvider = $produk->search(Yii::$app->request->queryParams);
        return $this->render('index',[
            'produkProvider' => $produkProvider
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact(){
        return $this->render('contact');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionSaran()
    {
        $model = new SaranSearch();
        $model->tanggal= date('Y-m-d');
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {

//            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
//                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
//            } else {
//                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
//            }

            return $this->refresh();
        } else {
            $list = $model->search(null);

            return $this->render('saran', [
                'model' => $model,
                'list' => $list,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Function actionInstallDb for
     * Usage: actionInstallDb()
     **/
    public function actionInstallDb()
    {
        $tmpPath = Yii::getAlias('@frontend/runtime/tmp');
        if(!defined('STDOUT')){
            define('STDOUT',fopen($tmpPath,'w'));
        }

        $migration = new MigrateController('migrate',Yii::$app);

//        $migration->runAction('down',[
//            'migrationPath' => '@common/migrations','interactive' => false
//        ]);

//        $migration->runAction('up',[
//            'migrationPath' => '@common/migrations','interactive' => false
//        ]);
//
//        $handler = fopen($tmpPath,'r');
//        $message = '';
//        while (($buffer = fgets($handler,4096)) !== false){
//            $message .= $buffer . '<br>';
//        }
//
//        fclose($handler);
//        $this->render('installdb',[
//            'message'=>$message
//        ]);

    }

    /**
     * Public function actionProfile() For
     **/
    public function actionProfile()
    {
        $model = UserFront::findOne(Yii::$app->user->id);
        $model->password = '';
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            if(trim($model->password) !=''){
                $model->setPassword($model->password);
            }else{
                $model->password = $model->oldAttributes['password'];
            }
            $model->save();
            $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    /**
     * Public function actionUpdateDB() For
     **/
    public function actionUpdateDb()
    {
        $this->updateDB();
    }

    /**
     * Function actionHapusDb for
     * Usage: actionHapusDb()
     **/
    public function actionHapusDb()
    {
//        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
//        $tables = Yii::$app->db->schema->getTableNames();
//        foreach ($tables as $table) {
//            Yii::$app->db->createCommand()->dropTable($table)->execute();
//        }
//        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }


    //=============================================================
    function addColumn($table,$column, $dataType){
        Yii::beginProfile('UpdateDB');
        $builder = $this->db->createCommand();
        $tableSchema = $this->db->schema->getTableSchema($table);
        if(is_null($tableSchema->getColumn($column))){
            $builder->addColumn($table,$column,$dataType);
            $builder->execute();
        };
        Yii::endProfile('UpdateDB');
    }

    function createTable($table,$columns){
        Yii::beginProfile('CreateTable');
        $queryBuilder = new QueryBuilder($this->db);
        $cols = [];
        foreach ($columns as $name => $type) {
            if (is_string($name)) {
                $cols[] = "\t" . $this->db->quoteColumnName($name) . ' ' . $queryBuilder->getColumnType($type);
            } else {
                $cols[] = "\t" . $type;
            }
        }
        $sql = 'CREATE TABLE IF NOT EXISTS ' . $this->db->quoteTableName($table) . " (\n" . implode(",\n", $cols) . "\n)";

        $builder = Yii::$app->db->createCommand($sql);
        $builder->execute();
        Yii::endProfile('CreateTable');
    }


    /**
     * Public function addFk() For
     **/
    public function addFk($name,$table,$column,$refTable,$refColumn,$onDelete = 'CASCADE',$onUpdate = 'CASCADE')
    {
        Yii::beginProfile('UpdateFK');
        $tableSchema = Yii::$app->db->schema->getTableSchema($table);
        Yii::trace(ArrayHelper::keyExists($name,$tableSchema->foreignKeys),'Fk');
        $builder = Yii::$app->db->createCommand();
        if(ArrayHelper::keyExists($name,$tableSchema->foreignKeys)){
            $builder->dropForeignKey($name,$table);
            $builder->execute();
        }
        $builder->addForeignKey($name,$table,$column,$refTable,$refColumn,$onDelete,$onUpdate);
        $builder->execute();
        Yii::endProfile('UpdateFK');
    }

    /**
     * Public function updateDB() For
     **/
    public function updateDB()
    {
        Yii::beginProfile('UpdateDB');
        $transaction = Yii::$app->db->beginTransaction();
        try{
            $this->addColumn('order','nama','string');
            $this->addColumn('order','alamat','string');
            $this->addColumn('order','no_telp','VARCHAR(20)');
            $this->addColumn('order','ongkir','DECIMAL(15,2)');
            $this->addColumn('order','kota','string');
            $this->addColumn('order','provinsi','string');
            $this->addColumn('order','provinsi','string');
            $this->addColumn('order','kode_pos','VARCHAR(10)');
            $this->addColumn('order','tgl_pembayaran','DATETIME');

            $this->addColumn('pelanggan','status','int');
            $this->addColumn('pelanggan','kota','string');
            $this->addColumn('pelanggan','provinsi','string');
            $this->addColumn('pelanggan','kode_pos','VARCHAR(10)');
            $this->addColumn('pelanggan','tgl_registrasi','datetime');
			
            $this->addColumn('kategori_produk','status','INT(1)');
			
            $this->createTable('transaksi',[
                'id_transaksi' => 'varchar(20) NOT NULL',
                'tanggal' => Schema::TYPE_DATE,
                'keterangan_transaksi' => 'varchar(100) NOT NULL',
                'total_transaksi' => Schema::TYPE_DECIMAL . '(15,2) NOT NULL',
                '`status` enum(\'Lunas\',\'Belum Lunas\')',
                '`tipe_transaksi` enum(\'Masuk\',\'Keluar\')',
                'no_bukti' => 'varchar(100)',
                'PRIMARY KEY (`id_transaksi`) USING BTREE'
            ]);

            $this->createTable('jurnal',[
                'id' => 'int(11) NOT NULL AUTO_INCREMENT',
                'id_transaksi' => 'varchar(20) NOT NULL',
                'keterangan' => 'varchar(255)',
                'tanggal' => Schema::TYPE_DATE,
                'images' => 'varchar(255)',
                'PRIMARY KEY (`id`) USING BTREE'
            ]);

            $this->createTable('jurnal_detail',[
                'id' => 'int(11) NOT NULL AUTO_INCREMENT',
                'id_jurnal' => 'int(11)',
                'kode_akun' => 'varchar(10)',
                'debit' => Schema::TYPE_DECIMAL . '(15,2) NOT NULL DEFAULT 0',
                'credit' => Schema::TYPE_DECIMAL . '(15,2) NOT NULL DEFAULT 0',
                'PRIMARY KEY (`id`) USING BTREE',
                'INDEX `fk_jurnal_detail_jurnal_1`(`id_jurnal`) USING BTREE',
                'INDEX `fk_jurnal_detail_akun_1`(`kode_akun`) USING BTREE',

            ]);



            $this->addFk('jurnal_ibfk_1','jurnal','id_transaksi','transaksi','id_transaksi');
            $this->addFk('fk_jurnal_detail_id_jurnal','jurnal_detail','id_jurnal','jurnal','id');
            $this->addFk('fk_jurnal_detail_kode_akun','jurnal_detail','kode_akun','akun','kode_rekening');

            $checkAkun = $this->db->createCommand("SELECT COUNT(*) FROM 'akun' WHERE kode_rekening = '5.5' AND nama_rekening ='Beban Lain-Lain'")->queryScalar();
            if(!$checkAkun){
                $builder = $this->db->createCommand();
                $builder->delete('akun','kode_rekening=\'5.5\'')->execute();
                $builder->insert('akun',[
                    'kode_rekening' => '5.5',
                    'nama_rekening' => 'Beban Lain-Lain',
                    'klasifikasi' => 'F',
                ])->execute();
            }

            $transaction->commit();
        }catch (\Exception $e ){
            Yii::error($e->getMessage(),'Update');
            $transaction->rollBack();
        }

        Yii::endProfile('UpdateDB');

        echo '<pre>';
        print_r(Yii::getLogger()->profiling);
        echo '</pre>';
    }


}
