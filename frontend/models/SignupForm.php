<?php
namespace frontend\models;

use yii\base\Model;
use frontend\models\UserFront as User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $password;
    public $nama;
    public $kota;
    public $provinsi;
    public $kode_pos;
    public $email;
    public $alamat;
    public $no_telp;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            [['alamat','nama','kota','provinsi','kode_pos'], 'required'],
            ['no_telp', 'required'],
            ['username', 'unique', 'targetClass' => '\frontend\models\UserFront', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\frontend\models\UserFront', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->nama = $this->nama;
        $user->alamat = $this->alamat;
        $user->kota = $this->kota;
        $user->provinsi = $this->provinsi;
        $user->kode_pos = $this->kode_pos;
        $user->no_telp = $this->no_telp;
        $user->setPassword($this->password);
//        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
